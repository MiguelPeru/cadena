package org.cuatrovientos.junit.ejercicio01;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CadenaTest {

	private Cadena target;
	@Before
	public void setUp() throws Exception {
		target = new Cadena();
	}
	
	@Test
	public void testLongitud() {
		int expected = 4;
		int actual = target.longitud("test");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testLongitudVacio() {
		int expected = 0;
		int actual = target.longitud("");
		assertEquals(expected, actual);
	}

	@Test
	public void testVocales() {
		int expected = 1;
		int actual = target.vocales("test");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testVocalesVacio() {
		int expected = 0;
		int actual = target.vocales("");
		assertEquals(expected, actual);
	}

	@Test
	public void testInvertir() {
		String expected = "tset";
		String actual = target.invertir("test");
		assertEquals(expected, actual);
	}
	
	@Test
	public void testInvertirVacio() {
		String expected = "";
		String actual = target.invertir("");
		assertEquals(expected, actual);
	}

	@Test
	public void testContarLetra() {
		int expected = 2;
		int actual = target.contarLetra("test", 't');
		assertEquals(expected, actual);
	}
	
	@Test
	public void testContarLetraVacio() {
		int expected = 0;
		int actual = target.contarLetra("", 't');
		assertEquals(expected, actual);
	}

}
