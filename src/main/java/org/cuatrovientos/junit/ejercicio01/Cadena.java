package org.cuatrovientos.junit.ejercicio01;

public class Cadena {
	public int longitud(String cadena) {
		return cadena.length();
	}
	
	public int vocales(String cadena) {
		int vocales = 0;
		for (int i = 0; i < this.longitud(cadena); i++) {
			switch (cadena.charAt(i)) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				vocales += 1;
				break;
			}
		}
		
		return vocales;
	}
	
	public String invertir(String cadena) {
		String inversa = "";
		
		for (int i = cadena.length()-1 ; i >= 0; i--) {
			inversa += cadena.charAt(i);
		}
		
		return inversa;
	}
	
	public int contarLetra(String cadena, char caracter) {
		int cuenta = 0;
		
		for (int i = 0; i < this.longitud(cadena); i++) {
			if (cadena.charAt(i) == caracter) {
				cuenta += 1;
			}
		}
		
		return cuenta;
	}
		
}
